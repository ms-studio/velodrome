<?php 

?>
<article class="post" id="post-<?php the_ID(); ?>">
  <header class="main-header">
    <h1 class="h1"><?php the_title(); ?></h1>
  </header>
	
	<div class="content">
  <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
	</div>

</article>
<?php

