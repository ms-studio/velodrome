<?php
/**
 */

get_header(); ?>

  <div id="main" class="main">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>">
      <header>
        <h2 class="h2"><?php the_title(); ?></h2>
      </header>
      
      <p class="attachment"><a href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'medium' ); ?></a></p>
      <p class="caption"><?php if ( !empty($post->post_excerpt) ) the_excerpt(); // this is the "caption" ?></p>

      <?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>

    </article>

  <?php endwhile; else: ?>

    <p>Sorry, no attachments matched your criteria.</p>

<?php endif; ?>

  </div>

<?php get_footer(); ?>
