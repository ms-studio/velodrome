<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */
 
// load_theme_textdomain( 'velodrome', get_template_directory() . '/languages' ); 
 
require_once('functions-init.php');

// require_once('modules/mem-extras/date-function.php');

require_once('modules/img-gallery/gallery-function.php');


/* CSS/JS Versioning
*********************/ 

// Custom Functions for CSS/Javascript Versioning
$GLOBALS["TEMPLATE_URL"] = get_bloginfo('template_url')."/";
$GLOBALS["TEMPLATE_RELATIVE_URL"] = wp_make_link_relative($GLOBALS["TEMPLATE_URL"]);

// Add ?v=[last modified time] to style sheets
function versioned_stylesheet($relative_url, $add_attributes=""){
  echo '<link rel="stylesheet" href="'.versioned_resource($relative_url).'" '.$add_attributes.'>'."\n";
}

// Add ?v=[last modified time] to javascripts
function versioned_javascript($relative_url, $add_attributes=""){
  echo '<script src="'.versioned_resource($relative_url).'" '.$add_attributes.'></script>'."\n";
}

// Add ?v=[last modified time] to a file url
function versioned_resource($relative_url){
  $file = $_SERVER["DOCUMENT_ROOT"].$relative_url;
  $file_version = "";

  if(file_exists($file)) {
    $file_version = "?v=".filemtime($file);
  }

  return $relative_url.$file_version;
}


/* Custom Search Form
*************************/

function my_search_form( $form ) {
    $form = '<form role="search" method="get" class="searchform" action="' . home_url( '/' ) . '" >
    <div>
    	<label>
    	<span class="screen-reader-text" for="s">'. __( 'Recherche pour:' ) .'</span>
    		<input type="text" class="searchfield" value="' . get_search_query() . '" name="s" id="s" placeholder="Recherche..." />
    		<input type="submit" class="submit" value="'. esc_attr__( 'Recherche' ) .'" />
    	</label>
    </div>
    </form>';

    return $form;
}

add_filter( 'get_search_form', 'my_search_form' );


/* Turn links into hyperlinks
******************************/

function vldr_process_hyperlinks($vldr_content) {
			
			$vldr_content = ' ' . $vldr_content;
			$attribs = ''; 
			$vldr_content = preg_replace(
				array(
					'#([\s>])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
					'#([\s>])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
					'#([\s>])([a-z0-9\-_.]+)@([^,< \n\r]+)#i'
					),
				array(
					'$1<a href="$2"' . $attribs . '>$2</a>',
					'$1<a href="http://$2"' . $attribs . '>$2</a>',
					'$1<a href="mailto:$2@$3">$2@$3</a>'),$vldr_content);
			$vldr_content = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $vldr_content);
			$vldr_content = trim($vldr_content);
			
			if ( function_exists('eae_encode_emails') ) {
					$vldr_content = eae_encode_emails($vldr_content);
			}
			
			return $vldr_content;
}

/* Generate markup for background images
******************************/

function vldr_filter_jpegs($var) {
		
		// this function filters an array an return JPEGs only.
		return ( is_array($var) && ($var["mime_type"] == "image/jpeg") );
}


function vldr_select_jpegs($acf_term_img) {
		// 1) run filter.
		$acf_term_jpg = array_filter($acf_term_img, "vldr_filter_jpegs");
		// 2) reset array numbering, discard empty slots.
		$acf_term_jpg = array_values($acf_term_jpg);
		return $acf_term_jpg;
}



function vldr_background_img($acf_term_img) {
		
		// step 1)
		// filter array - has been done already.
	
		// step 2)
		// randomize output
		shuffle($acf_term_img);
		
		// step 3) 
		// select item
		
		if ( isset($acf_term_img[0]["sizes"]["superwide"]) ) {
			$acf_superwide = $acf_term_img[0]["sizes"]["superwide"];
		} else if ( isset($acf_term_img[0]["sizes"]["full"]) ) {
			$acf_superwide = $acf_term_img[0]["sizes"]["full"];
		}
		
		if (isset ($acf_superwide) ) {
		?>
		<style>
		html {
			background: url('<?php echo $acf_superwide; ?>') no-repeat center center fixed;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
		}
		</style>
		<?php
		
		}
		
		// info for IE compatibility : 
		// http://css-tricks.com/perfect-full-page-background-image/
}


function vldr_generate_background($acf_term_img,$queried_object) {
			
				
			
			if (is_array($acf_term_img)) {
			  // filter for JPEGs
			  $acf_term_jpg = vldr_select_jpegs( $acf_term_img );
			}
			
			if ( empty($acf_term_jpg) ) {
				
					// 1) TEST PARENT	
				
					if ( $queried_object->parent != 0 ) {
			
						// 1) try for parent term
						$acf_parent_term_img = get_field('acf_lieux_img', 'locaux_' . $queried_object->parent);
						
						// 2) filter for JPEGs
						$acf_term_jpg = vldr_select_jpegs( $acf_parent_term_img );
						
	//					echo '<pre class="hidden admin-show"> $acf_term_jpg: ';
	//					var_dump($acf_term_jpg);
	//					echo '</pre>';
					} // END testing the Parent Object
			}		
				
			if ( empty($acf_term_jpg) ) {
				
						// 2) Still no JPEG? Get a random background
						
						// get a random background
						
						// 1) get default background
						$acf_random_img = get_field('acf_images_de_fond', 'option');
						
						// 2) filter for JPEGs
						$acf_term_jpg = vldr_select_jpegs( $acf_random_img );
						
//						if ( current_user_can('activate_plugins') ) {
//						 // user is admin
//						 echo "<p>Debug: We loaded a random image.</p>";
//						}
						
						
				}	// now we are pretty sure we have an image...
					
			// 3) if defined, generate output.
				
			if ($acf_term_jpg) {
					
						vldr_background_img($acf_term_jpg);
					
			}
					
//			echo '<pre>';
//			var_dump($acf_term_jpg);
//			echo '</pre>';

 // // JPEG defined = generate output.
 // vldr_background_img($acf_term_jpg);
			
} // END function vldr_generate_background


function vldr_filter_svgs($var) {

		// this function will filer an array an return JPEGs only.
		
		return ( is_array($var) && ($var["mime_type"] == "image/svg+xml") );
}

function vldr_select_svgs($acf_term_img) {
		
		if (is_array($acf_term_img)) {
		  // 1) run filter.
		  $acf_term_svg = array_filter($acf_term_img, "vldr_filter_svgs");
		  // 2) reset array numbering, discard empty slots.
		  $acf_term_svg = array_values($acf_term_svg);
		} else {
				$acf_term_svg = array();
		}
		return $acf_term_svg;
}


function vldr_generate_svg($acf_term_img,$queried_object) {
		
		if (is_array($acf_term_img)) {
		  // filter for SVGs
		  $acf_term_svg = vldr_select_svgs( $acf_term_img );
		}
		
		if (isset($acf_term_svg)) {
			
			// if defined, generate output.
			// vldr_svg_display($acf_term_svg);
			
		} else if ( $queried_object->parent != 0 ) {
		
				// 1) try for parent term
				$acf_parent_term_img = get_field('acf_lieux_img', 'locaux_' . $queried_object->parent);
				
				// 2) filter for JPEGs
				$acf_term_svg = vldr_select_svgs( $acf_parent_term_img );
				
				// 3) if defined, generate output.
				if ($acf_term_svg) {
					// vldr_svg_display($acf_term_svg);
				}
				
		} else {
		
				$acf_term_svg = array();
		// no SVG available
		
		}
		
		return $acf_term_svg;
		
}

function vldr_svg_display($acf_term_svg) {

	// display item
	
	if ( isset( $acf_term_svg[0]["url"] ) ) {
	
//			echo '<pre>';
//			var_dump($acf_term_svg);
//			echo '</pre>';

				// get the width and height
				
				$xmlget = simplexml_load_file($acf_term_svg[0]["url"]);
				$xmlattributes = $xmlget->attributes();
				$svg_width = (string) $xmlattributes->width; 
				$svg_height = (string) $xmlattributes->height;

		?>
		<style>
		.single-membre {
			background-image: url('<?php echo $acf_term_svg[0]["url"]; ?>');
			background-position: 0% 0%;
			background-repeat: no-repeat;
			background-size: 100% auto;
			background-origin: padding-box;
		}
		</style>
		<?php
	
	}
	
}


/**
 * Searches for plain email addresses in given $string and
 * encodes them (by default) with the help of eae_encode_str().
 * 
 * Regular expression is based on based on John Gruber's Markdown.
 * http://daringfireball.net/projects/markdown/
 * 
 * @param string $string Text with email addresses to encode
 * @return string $string Given text with encoded email addresses
 */
  
function eae_encode_emails($string) {

	// abort if $string doesn't contain a @-sign
	if (apply_filters('eae_at_sign_check', true)) {
		if (strpos($string, '@') === false) return $string;
	}

	// override encoding function with the 'eae_method' filter
	$method = apply_filters('eae_method', 'eae_encode_str');

	// override regex pattern with the 'eae_regexp' filter
	$regexp = apply_filters(
		'eae_regexp',
		'{
			(?:mailto:)?
			(?:
				[-!#$%&*+/=?^_`.{|}~\w\x80-\xFF]+
			|
				".*?"
			)
			\@
			(?:
				[-a-z0-9\x80-\xFF]+(\.[-a-z0-9\x80-\xFF]+)*\.[a-z]+
			|
				\[[\d.a-fA-F:]+\]
			)
		}xi'
	);

	return preg_replace_callback(
		$regexp,
		create_function(
            '$matches',
            'return '.$method.'($matches[0]);'
        ),
		$string
	);

}

/**
 * Encodes each character of the given string as either a decimal
 * or hexadecimal entity, in the hopes of foiling most email address
 * harvesting bots.
 *
 * Based on Michel Fortin's PHP Markdown:
 *   http://michelf.com/projects/php-markdown/
 * Which is based on John Gruber's original Markdown:
 *   http://daringfireball.net/projects/markdown/
 * Whose code is based on a filter by Matthew Wickline, posted to
 * the BBEdit-Talk with some optimizations by Milian Wolff.
 *
 * @param string $string Text with email addresses to encode
 * @return string $string Given text with encoded email addresses
 */
function eae_encode_str($string) {

	$chars = str_split($string);
	$seed = mt_rand(0, (int) abs(crc32($string) / strlen($string)));

	foreach ($chars as $key => $char) {

		$ord = ord($char);

		if ($ord < 128) { // ignore non-ascii chars

			$r = ($seed * (1 + $key)) % 100; // pseudo "random function"

			if ($r > 60 && $char != '@') ; // plain character (not encoded), if not @-sign
			else if ($r < 45) $chars[$key] = '&#x'.dechex($ord).';'; // hexadecimal
			else $chars[$key] = '&#'.$ord.';'; // decimal (ascii)

		}

	}

	return implode('', $chars);

}


/* login interface
******************************/

//custom Login
function custom_login() { 
echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/login/login.css" />'; 
}
add_action('login_head', 'custom_login');

// src: http://codex.wordpress.org/Customizing_the_Login_Form

// change the link values so the logo links to your WordPress site
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


/* produire le nom du local
*******************************/

function vlrd_nom_local($nom_local) {
	
	$pos = strpos($nom_local, "-");
      		
	if ($pos === false) {
	          
  } else {
  
      $fir = $first = explode( "-", $nom_local ); 
      unset( $fir[0] ); 
      $end = ltrim( implode( "-", $fir ) ); 

      $nom_first = substr($nom_local, 0, $pos);
      
      $nom_local =  $nom_first . '<span class="small-print">' . substr($nom_local, $pos) . '</span>';
	
	 }
	
	return $nom_local;
}


/**
 * Processing "Last name, First name" pairs
 *
 * @param string $nom_prenom : The original post title ("Wilde, Oscar").
 *
 * @return string : The human-readable title ("Oscar Wilde").
 * 
 * Usage: 
 * $post_title = firstname_lastname(get_the_title());
 *
 * URL: https://gist.github.com/ms-studio/8793349
 *
 */

function vlrd_nom_prenom($post_title) {
	
	$pos = strpos($post_title, ",");
      		
	if ($pos === false) {
	  // nothing to do.
  } else {
      $fir = explode( ",", $post_title ); 
      unset( $fir[0] ); 
      $end = ltrim( implode( ",", $fir ) ); 
      $first_name = substr($post_title, 0, $pos);
      $pos++;
      $post_title = substr($post_title, $pos) . ' ' . $first_name;
	 }
	
	return $post_title;
}


function vlrd_news_p2p() {
		
		// Find connected people
		
		$p2p_person_link = '';
		
		     $connected_stuff = new WP_Query( array(
		      'post_type' => 'membres',
		      'posts_per_page' => -1,
		      'connected_type' => 'projects_to_people',
		      'nopaging' => true,
		      'connected_items' => get_the_ID(), 
		      ) );
		      
		      if ( $connected_stuff->have_posts() ) :
		      
		      		$p2p_person_link .= '<div class="news-item-ppl strong">';
		      
		      	     while ( $connected_stuff->have_posts() ) : $connected_stuff->the_post(); 
		      	     
		      	     			// define hyperlink field
		      	     			
		      	     			$p2p_person_link .= '<a class="u-h" href="'. get_permalink() .'">';
		      	     			
				      	      $nom_prenom = vlrd_nom_prenom(get_the_title());
				      	      
				      	      $p2p_person_link .= $nom_prenom;
				      	      
				      	      $p2p_person_link .= '</a>';
				      	      
		      	     // END OF LOOP
		      	     endwhile;
		      	     
		      	     $p2p_person_link .= '</div>';
		      	     
		     // wp_reset_postdata();
//		     		wp_reset_query();
		     endif;
		     
		     return $p2p_person_link;

}


/* Archive Query

* when visiting the term archive for "local"
* modify the archive query: 
* order by Name, not by date.

* Note: this messes up the page title: vldr_page_title()

*********************/

function archive_page_query($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    
    if ( is_year() ) {
    	
    	global $wp_query;
    	
    	// $query->init();
    	
    	$the_query_year = get_query_var('year');
    	set_query_var('year', null);
    	
    	set_query_var('posts_per_page', -1);
    	set_query_var('meta_key', '_mem_start_date');
    	set_query_var('meta_query', array(
	        'relation' => 'AND',
	        array(
	            'key' => '_mem_start_date',
	            'value' => $the_query_year,
	            'compare' => '>='
	        ),
	        array(
	            'key' => '_mem_start_date',
	            'value' => $the_query_year.'-12-32',
	            'compare' => '<='
	        )
    	));
    	set_query_var('orderby', 'meta_value');
    	set_query_var('order', 'DESC');
    	
    } else if (is_archive()) {
   // utilisé pour les locaux, collectifs
      set_query_var('orderby', 'title');
      set_query_var('order', 'ASC');
      set_query_var('posts_per_page', -1);

    }
  }
}

add_action('pre_get_posts','archive_page_query');


/* Bloginfo Shortcode

* http://css-tricks.com/snippets/wordpress/bloginfo-shortcode/
* usage: [bloginfo key='version']

*********************/

function digwp_bloginfo_shortcode( $atts ) {
   extract(shortcode_atts(array(
       'key' => '',
   ), $atts));
   return get_bloginfo($key);
}
add_shortcode('bloginfo', 'digwp_bloginfo_shortcode');



/**
 * Flush our Transients
 *
 * http://codex.wordpress.org/Transients_API
 * http://codex.wordpress.org/Plugin_API/Action_Reference
 *
 */
 
 function edit_custom_transients() {
      delete_transient( 'page_membres' );
      delete_transient( 'page_news' );
 }
 add_action( 'edit_post', 'edit_custom_transients' );


/* Curl helper function
 * for Vimeo, SoundCloud, etc
*******************************/

function curl_get($url) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	// curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
	// include optional proxy information
	// four relative steps = WP base directory
	// if (file_exists(__DIR__.'../../../../../wp-proxy.php')) { 
		// require_once(__DIR__.'../../../../../wp-proxy.php');
	// }
	$return = curl_exec($curl);
	curl_close($curl);
	return $return;
}


// vimeo testing function
// source: http://stackoverflow.com/questions/11304044/determining-the-vimeo-source-by-url-regex

function discoverVimeo($url) {
    if ( ( ( $url = parse_url($url) ) !== false ) ) {
        $url = array_filter(explode('/', $url['path']), 'strlen');
        if (in_array($url[1], array('album', 'channels', 'groups')) !== true) {
            if ( is_numeric($url[1]) ) {
            	array_unshift($url, 'item');
            } else {
            	array_unshift($url, 'user');
            }
        } 
        return array('type' => rtrim(array_shift($url), 's'), 'id' => array_shift($url));
    } 
    return false;
}



/* admin interface
******************************/

if ( is_user_logged_in() ) {
		require_once('functions-admin.php');
}




// end of functions.php