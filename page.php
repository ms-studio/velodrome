<?php
/**
 */

get_header(); ?>

<div id="main" role="main" class="main">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <article class="post" id="post-<?php the_ID(); ?>">
    <header class="main-header">
      <h1 class="h1"><?php the_title(); ?></h1>
    </header>
  	
  	<div class="content">
    <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
  	</div>
  
  </article>
  <?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
