<?php
/**
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8"> 
  <title><?php wp_title( '', true, 'right' ); ?></title>
  
  <?php // ** DESCRIPTION v.0.4 **
  
  if ( is_front_page() ) {
  	?><meta name="description" content="Le Vélodrome est un espace artistique et artisanal autogéré situé en plein cœur du quartier de la Jonction à Genève." />
  	<?php
  } else if (is_single() || is_page() ) {
	  	if ( have_posts() ) : while ( have_posts() ) : the_post(); 
	  ?><meta name="description" content="<?php  
	  	$descr = get_the_excerpt();
	  	$text = str_replace( '/\r\n/', ', ', trim($descr) ); 
	  	echo esc_attr($text);
	  ?>" />
	  <?php endwhile; endif; 
  } 
  ?>
 
  <meta name="author" content="Association Le Vélodrome">
  
  <?php // ** SEO OPTIMIZATION v.0.2 **
  			if ( is_attachment() ) {
  ?><meta name="robots" content="noindex,follow" /><?php
  } else if( is_single() || is_page() || is_home() ) { 
  ?><meta name="robots" content="all,index,follow" /><?php 
  } else if ( is_category() || is_archive() ) { 
  ?><meta name="robots" content="noindex,follow" /><?php } 
  ?>
  	  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="/favicon.ico?v2" type="image/vnd.microsoft.icon" />
  <link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
    
  <style>.hidden {display: none;}</style>
  
  <link rel="alternate" type="application/rss+xml" title="RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

  <?php wp_head(); ?>

</head>
<body <?php 

		// init variable
		$nfo_body_var = '';
		
		if (is_single()) {
							
			// test categories
			include( get_template_directory() . '/inc/categories-list.php' );
					
		}

		$nfo_body_var.= 'no-js';
		
		body_class($nfo_body_var);

	?>>
  <div id="container" class="container clear">
    <header role="banner" class="header site-header clear">
      <h1 class="h1"><a class="vldr-logo icon-vldr" href="<?php echo get_option('home'); ?>/">Vldr</a></h1>
      <?php 
            
      if ( is_front_page() ) {
      	?>
      		<div class="description hidden-mob"><p class="description-inside">
      			<?php the_field('acf_infos_adresse', 'option'); ?>
      		</p></div>
      	<?php
      }
      
       ?>
    </header>
    
    <nav class="main-nav">
    	<h2 id="nav-menu-toggle" class="h2 menu-title"><span class="menu-closed icon-menu-alt inside">M</span></h2>
	    <?php wp_nav_menu( array(
	    	 'theme_location' => 'primary',
	    	 'container'       => 'false',
	    	 'menu_class'  => 'menu hidden-mob', // clear main-menu default-menu horiz-list small-font 
	    	 'depth'           => 0,
	    	 //'link_after'       => '&nbsp;',
	    	 ) ); ?>
    </nav>
    
    <div id="search-container" class="search-container">
	    <div class="search-toggle">
	    				<a href="#main-search" class="icon-search inside">R</a>
	    </div>
	    <div id="main-search" class="main-search search-box-wrapper js-hidden">
			    <?php  
			    get_search_form(); 
			    ?>    
			</div>
		</div>

	