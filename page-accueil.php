<?php
/**
 */

get_header(); ?>

<div id="main" role="main" class="main">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <article class="plan-container" id="plan-velodrome">
    
  <ul class="plan-lo">
    	<?php 
    	
    	// plan-lo = liste locaux
    	      		
    	   $args = array(
    	   	'orderby'                  => 'name',
    	   	'order'                    => 'ASC',
    	   	'hide_empty'               => 0,
    	   	'hierarchical'             => 1,
    	   	'exclude'                  => '',
    	   	'include'                  => '',
    	   	'number'                   => '',
    	   	'taxonomy'                 => 'locaux',
    	   );
    	   
    	   // see http://codex.wordpress.org/Function_Reference/get_categories
    	   
    	   // hierarchical = When true, the results will include sub-categories that are empty, as long as those sub-categories have sub-categories that are not empty.
    	   
    	   // parent = when the value is set to zero, we get the top level categories only.
    	   
    	$liste_des_locaux = get_categories( $args );
    	
    	
    	
    	foreach ( $liste_des_locaux as $local ) {
    		
    		$slug_local = $local->slug;
    		$nom_local = $local->name;
    		
    		$nom_local = vlrd_nom_local($nom_local);
    		      		
    		echo '<li class="plan-lo-item lo-'.$slug_local.'">';
    		echo '<a href="' . get_term_link( $slug_local, 'locaux' ) . '">' . $nom_local . '</a>';
    		
    		echo '</li>
    		'; // end .li-lo-item
    			
    		} // end foreach
    		
    		?>
    
    </ul>
    <div class="plan-velodrome"></div>
  </article>
  <?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
