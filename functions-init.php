<?php  

// Initialize Theme

/* 1. Register CSS and JS
 * 2. Define theme support, menus, image sizes
 * 3. Custom post types and Taxonomies
 * 4. Date variables for MEM plugin
 * 5. 
**************/

// Change-Detector-XXXXXXX - for Espresso.app


/* Allow Automatic Updates
 ******************************
 * http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */

add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
add_filter( 'allow_major_auto_core_updates', '__return_true' );

// Title

add_filter( 'wp_title', 'vldr_page_title', 10, 2 );

if ( ! function_exists( 'vldr_page_title' ) ) :
	/**
	 * Adjust the page title to something sensible
	 */
	function vldr_page_title( $title, $sep ) {
		// We don't want to affect RSS feeds
		if ( is_feed() ) { 
			return $title; 
		}

		if ( is_front_page() ){
		
			return 'Association Le Vélodrome, Genève';
		
		} elseif ( is_404() ) {
		
			return 'Page introuvable &mdash; ' . get_bloginfo( 'name' );
		
		} elseif ( is_search() ) {
		
			return 'Résultats de la recherche &mdash; ' . get_bloginfo( 'name' );
		
		} elseif ( is_year() ) {
			
			$vldr_urlpath = $_SERVER['REQUEST_URI'];
			return 'Archives ' . substr($vldr_urlpath, -5, -1) . ' &mdash; ' . get_bloginfo( 'name' );
		
		} elseif ( is_tax( 'locaux' ) ) {
		
			$this_term = get_query_var( 'term' );
			// get_query_var( 'taxonomy' );
			return 'Local ' . $this_term . ' &mdash; ' . get_bloginfo( 'name' );
		
		} elseif ( is_singular( 'membres' ) ) {
		
			return vlrd_nom_prenom( $title ) . ' &mdash; ' . get_bloginfo( 'name' );
		
		} else { 
			
			$title = ucfirst($title);
			return $title . ' &mdash; ' . get_bloginfo( 'name' ); 
		
		}
	}

endif; // ttf_common_page_title



  // Unregister jQuery 
  // (we call it in the footer)
  // ****************************
   
function custom_register_styles() {
				
				
$ms_dev_mode = 'prod'; // dev or prod				
				
				/**
				 * Custom CSS
				 */
				 
				// the WebFonts stylesheet
				wp_enqueue_style( 
						'webfonts', 
						get_stylesheet_directory_uri() . '/fonts/fonts.css',
						false, // dependencies
						null // version
				); 
				
				if ( $ms_dev_mode == 'dev' ) {
				
						// DEV: the MAIN stylesheet - uncompressed
						wp_enqueue_style( 
								'main_css_style', 
								get_stylesheet_directory_uri() . '/css/dev/00-main.css', // main.css
								false, // dependencies
								null // version
						); 
				
				} else {
				
						// PROD: the MAIN stylesheet - combined and minified
						wp_enqueue_style( 
								'main_css_style', 
								get_stylesheet_directory_uri() . '/css/build/styles.20170520224103.css', // main.css
								false, // dependencies
								null // version
						); 
				}
				

				/**
				 * Custom JavaScript
				 */
	      	      	      
	      wp_enqueue_script( 
	      		'modernizer_js', // handle
	      		get_stylesheet_directory_uri() . '/js/libs/modernizr.custom.14446.min.js', //src
	      		false, // dependencies
	      		null, // version
	      		false // in footer
	      );
	      
	      wp_deregister_script('jquery');
	      	wp_register_script(
	      		'jquery', 
	      		get_site_url() . '/wp-includes/js/jquery/jquery.js', 
	      		false, // dep
	      		'1.10.2', // jquery version
	      		true // load in footer !!!
	      	);
	      	wp_enqueue_script('jquery');

	      
	      if ( $ms_dev_mode == 'dev' ) {
	      
				      wp_enqueue_script( 
				      // the MAIN JavaScript file -- development version
				      		'main_js',
				      		get_stylesheet_directory_uri() . '/js/script.js', // scripts.js
				      		array('jquery'), // dependencies
				      		null, // version
				      		true // in footer
				      );
	      
	      } else {
	      
	      			wp_enqueue_script( 
	      			// the COMPRESSED JavaScript file 
	      					'main_js',
	      					get_stylesheet_directory_uri() . '/js/build/scripts.20170520224103.js', // scripts.js
	      					array('jquery'), // dependencies
	      					null, // version
	      					true // in footer
	      			);
	      			
	      }
	      
	      wp_enqueue_script( 
	       		'swipebox',
	       		get_stylesheet_directory_uri() . '/js/libs/swipebox/src/js/jquery.swipebox.min.js',
	       		array('jquery'),
	       		'1.2.7', // version
	       		true // in footer
	       ); 
	      
}
add_action( 'wp_enqueue_scripts', 'custom_register_styles', 10);



/* Some header cleanup 
******************************/

remove_action('wp_head', 'shortlink_wp_head');

remove_action('wp_head', 'feed_links' ); // not working...
remove_action('wp_head', 'feed_links', 2 );
remove_action('wp_head', 'feed_links_extra', 3);
// in order to remove the comments feed. need to add manually the main RSS feed to the header.

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

// Prevents WordPress from testing ssl capability on domain.com/xmlrpc.php?rsd
remove_filter('atom_service_url','atom_service_url_filter'); 

 
/* Post-Thumbnails Support
******************************/
 
 if ( function_exists( 'add_theme_support' ) ) {
//	 	add_theme_support( 'post-thumbnails' );
//     set_post_thumbnail_size( 150, 150 ); // default Post Thumbnail dimensions  
    // more info: http://codex.wordpress.org/Post_Thumbnails 
 }

/* Search Form Markup
******************************/

add_theme_support('html5', array('search-form'));


/* Custom image sizes
******************************/ 
 
 if ( function_exists( 'add_image_size' ) ) { 
 	//add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
 	//add_image_size( 'landscape', 304, 184, true ); // true = cropped
 	 
 	 add_image_size( 'retina_square', 520, 520, true );
 	 add_image_size( 'superwide', 2048, 2048, false );	
 	 
 }
 
 /* Custom image quality
  * using this plugin:
  * https://github.com/StickyBeat/custom-image-size-quality
 ******************************/ 
 
if ( function_exists( 'set_image_size_quality' ) ) { 
		set_image_size_quality( 'superwide', 60 );
		set_image_size_quality( 'large', 80 );
		set_image_size_quality( 'medium', 80 );
} 
 
 
 
 /* Custom Menus
 ******************************/ 
 
  if ( function_exists( 'register_nav_menus' ) ) {
  	register_nav_menus(
  			array(
  				 'primary' => __( 'Navigation Principale' ),
//  				 'second-menu' => __( 'Menu N°2' ),
//  				 'third-menu' => __( 'Menu N°3' ),
  				)
  			);
  }
  
/* Widget Area
******************************/ 

register_sidebar( array(
	'name'          => 'Primary Sidebar',
	'id'            => 'sidebar-1',
	'description'   => 'Main sidebar.',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h1 class="widget-title">',
	'after_title'   => '</h1>',
) ); 

/* Allowed FileTypes
 ********************
 * method based on 
 * http://howto.blbosti.com/?p=329
 * List of defaults: https://core.trac.wordpress.org/browser/tags/3.8.1/src/wp-includes/functions.php#L1948
*/
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

		// add your extension to the array
		$existing_mimes['svg'] = 'image/svg+xml';

		// removing existing file types
		unset( $existing_mimes['bmp'] );
		unset( $existing_mimes['tif|tiff'] );

		// and return the new full result
		return $existing_mimes;
}
  

/* Custom taxonomies
 ********************
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
*/

function custom_taxonomies() 
{  	 
	
	register_taxonomy('locaux',
			array( 'membres' ),
			array( 
	 		'hierarchical' => true, 
	 		'label' => 'Locaux',
	 		'labels'  => array(
	 			'name'                => _x( 'Locaux', 'taxonomy general name' ),
	 			'singular_name'       => _x( 'Local', 'taxonomy singular name' ),
	 			'search_items'        => __( 'Chercher parmi les locaux' ),
	 			'popular_items'              => __( 'Les plus utilisés' ),
	 					'all_items'                  => __( 'Tous les locaux' ),
	 					'parent_item'                => null,
	 					'parent_item_colon'          => null,
	 					'edit_item'                  => __( 'Modifier local' ),
	 					'update_item'                => __( 'Mettre à jour le local' ),
	 					'add_new_item'               => __( 'Nouveau local' ),
	 					'new_item_name'              => __( 'Nouveau local' ),
	 					'separate_items_with_commas' => __( 'Séparez les locaux par des virgules' ),
	 					'add_or_remove_items'        => __( 'Ajouter ou supprimer des locaux' ),
	 					'choose_from_most_used'      => __( 'Choisir parmi les locaux les plus utilisés' ),
	 					'not_found'                  => __( 'Aucun local trouvé.' ),
	 			'menu_name'           => __( 'Locaux' )
	 		),
	 		'show_ui' => true,
	 		'query_var' => true,
	 		'rewrite' => array('slug' => 'local'),
	 		'singular_label' => 'Local') 
	 );
	 
	 register_taxonomy('collectifs',
	 		array( 'membres' ),
	 		array( 
	  		'hierarchical' => true, 
	  		'label' => 'Collectifs',
	  		'labels'  => array(
	  			'name'                => _x( 'Collectifs', 'taxonomy general name' ),
	  			'singular_name'       => _x( 'Collectif', 'taxonomy singular name' ),
	  			'search_items'        => __( 'Chercher parmi les collectifs' ),
	  			'popular_items'              => __( 'Les plus utilisés' ),
	  					'all_items'                  => __( 'Tous les collectifs' ),
	  					'parent_item'                => null,
	  					'parent_item_colon'          => null,
	  					'edit_item'                  => __( 'Modifier collectif' ),
	  					'update_item'                => __( 'Mettre à jour le collectif' ),
	  					'add_new_item'               => __( 'Nouveau collectif' ),
	  					'new_item_name'              => __( 'Nouveau collectif' ),
	  					'separate_items_with_commas' => __( 'Séparez les collectifs par des virgules' ),
	  					'add_or_remove_items'        => __( 'Ajouter ou supprimer des collectifs' ),
	  					'choose_from_most_used'      => __( 'Choisir parmi les plus utilisés' ),
	  					'not_found'                  => __( 'Aucun collectif trouvé.' ),
	  			'menu_name'           => __( 'Collectifs' )
	  		),
	  		'show_ui' => true,
	  		'query_var' => true,
	  		'rewrite' => array('slug' => 'collectif'),
	  		'singular_label' => 'Collectif') 
	  );
	


//  	register_taxonomy('reglages',array (
//  	  0 => 'post',
//  	),array( 
//  		'hierarchical' => true, 
//  		'label' => 'Réglages',
//  		'show_ui' => true,
//  		'query_var' => true,
//  		'rewrite' => array('slug' => ''),
//  		'singular_label' => 'Réglage') 
//  	);
  

  		register_post_type(
  			'membres', array(	
  				'label' => 'Membres',
  				'description' => '',
  				'public' => true,
  				'show_ui' => true,
  				'show_in_menu' => true,
  				'menu_icon' => 'dashicons-groups',
  				'capability_type' => 'page',
  				'hierarchical' => false,
  				'rewrite' => array('slug' => 'profil'),
  				'query_var' => true,
  				'exclude_from_search' => false,
  				'menu_position' => 6,
  				'supports' => array('title','editor','custom-fields','revisions','thumbnail'),
  				'taxonomies' => array('locaux'),
  				'labels' => array (
  			  	  'name' => 'Membres',
  			  	  'singular_name' => 'Membre',
  			  	  'menu_name' => 'Membres',
  			  	  'add_new' => 'Ajouter',
  			  	  'add_new_item' => 'Ajouter un membre',
  			  	  'edit' => 'Modifier',
  			  	  'edit_item' => 'Modifier',
  			  	  'new_item' => 'Nouveau Membre',
  			  	  'view' => 'Afficher',
  			  	  'view_item' => 'Afficher le membre',
  			  	  'search_items' => 'Rechercher',
  			  	  'not_found' => 'Aucun résultat',
  			  	  'not_found_in_trash' => 'Aucun résultat',
  			  	  'parent' => 'Élément Parent',
  			),) );
  
} // end custom_taxonomies()  function
 //hook into the init action and call custom_taxonomies() when it fires
 add_action( 'init', 'custom_taxonomies', 0 ); 



/*
 * P2P - connect Posts and People (membres)
*/

// post_2_posts plugin
// https://github.com/scribu/wp-posts-to-posts/wiki/Basic-usage

function my_connection_types() {
	// Make sure the Posts 2 Posts plugin is active.
	if ( !function_exists( 'p2p_register_connection_type' ) )
		return;

	p2p_register_connection_type( array(
		'name' => 'projects_to_people',
		'from' => 'post',
		'to' => 'membres',
		'reciprocal' => true,
		'title' => 'Projets - Personnes',
		// 'admin_box' => 'from',
	) );
	
}
add_action( 'init', 'my_connection_types', 100 );

  
/*
 * ACF Options Page
 ************************
 * http://www.advancedcustomfields.com/resources/options-page/
*/

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
			'page_title' 	=> 'Options Vélodrome',
			'menu_title'	=> 'Options',
			'menu_slug' 	=> 'options-velodrome',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	
}  
  

/*
 * Minimalistic Event Manager
*/

function my_mem_settings() {
	mem_plugin_settings( array( 'post' ), 'full' );
}
add_action( 'mem_init', 'my_mem_settings' );









