<?php
/**
 */

get_header(); ?>

<div id="main" role="main" class="main">

<?php if (have_posts()) : while (have_posts()) : the_post(); 

if ( 'membres' == get_post_type() ) {
			
				include( get_template_directory() . '/single-membre.php' ); 
				
	} else if ( 'post' == get_post_type() ) {
						
				include( get_template_directory() . '/single-default.php' );
							
	} else {
			
				include( get_template_directory() . '/single-other.php' );
				
}

 endwhile; else: ?>

  <p>Aucun contenu trouvé.</p>

<?php endif; ?>

</div>

<?php get_footer(); ?>
