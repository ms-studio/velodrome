<?php
/**
 * Template Name: Liste des membres (par nom)
 */

get_header(); ?>

<div id="main" role="main" class="main page-membres">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div class="post" id="post-<?php the_ID(); ?>">
    <header class="hidden">
      <h1 class="h1"><?php the_title(); ?></h1>
    </header>
  
    <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); 
    
    // listing of Tags of taxonomy "locaux".
    
    ?>
    <?php endwhile; endif; ?>
    
    <div class="li-lo">
      	<?php 
      	
      	// li-lo = liste locaux
      		      		
      		echo '<div class="li-lo-item">';
      		// echo '<h2 class="h2 li-lo-h">titre</h2>';
      			
      		// 1: 
      		// Query for linked articles of post type = "membre".
      		
      		if ( false === ( $array_membres = get_transient('page_membres') ) ) {
      		
				      		$array_membres = array();
				      				
				  				$custom_query = new WP_Query( array(
					      		 		'post_type' => array( 'membres' ),
					      		 		'posts_per_page' => -1,
					      		 		'orderby'  => 'title',
					      		 		'order'  => 'ASC',
					      		 		));
					      		
					      		if ( $custom_query->have_posts() ) :
					      				  	while( $custom_query->have_posts() ) : $custom_query->the_post();
					      				  	
					      				  		// put into array.
					      				  		$array_membres[] = array( 
					      				  		    	"permalink" => get_permalink(),
					      				  		    	"title" => get_the_title(),
					      				  		    	"lowercase" => strtolower( get_the_title() ),
					      				  		 );
					      				  	
					      				  	endwhile; 
					      		endif;
					      		
					      		// 2: 
					      		// Query for Collectives
					      				
					      				
					      				$args = array(
					      				    'show_option_all'    => '',
					      				    	'style'              => 'list',
					      				    	'taxonomy'           => 'collectifs',
					      				    	'walker'             => null
					      				   ); 
					      				
					      				$collectifs = get_categories( $args );
					      				
					      				if ($collectifs) {
					      				    	foreach($collectifs as $term) { 
					      				    	    // put into array.
					      				    	    $array_membres[] = array( 
					      				    	        	"permalink" => get_term_link( $term->slug, 'collectifs' ),
					      				    	        	"title" => mb_strtoupper( $term->name, "UTF-8" ), // $term->name
					      				    	        	"lowercase" => mb_strtolower( $term->name, "UTF-8" ),
					      				    	     );
					      				    	} 
					      				}
					      		
					      		
					      		// 3:
					      		// Order the array
					      		
					      		function multi_array_sort($a,$b) {
					      		     //return $a['title']>$b['title'];
					      		     return strcmp($a["lowercase"], $b["lowercase"]);
					      		}
					      		usort($array_membres, "multi_array_sort");
					      		
					      		// 4:
					      		// put into transient
					      		
					      		set_transient( 'page_membres', $array_membres, 12 * HOUR_IN_SECONDS ); // 12 * HOUR_IN_SECONDS 
	      		
	      		} // end testing for transient
	      		
	      		// 5:
	      		// some init:
	      		if ( !empty ($array_membres) ) :
	      				  	
	      				  	$initialcounter = 1;
	      				  	$prev_initiale = '';
	      		
	      		// 5: Output array
	      				  	
	      				?>
	      				<ul class="ul-membres ul-clean">
	      				<?php 
	      				  	
	      						  foreach ($array_membres as $key => $item) {
	      						  
			      						  // Tester l'initiale.
			      						  $nom_entier = $array_membres[$key]["title"] ;
			      						  $nom_initiale = strtolower(mb_substr($nom_entier,0,1, "utf-8"));
			      						  
			      						  if (in_array($nom_initiale, array("i","j"))) {
			      						  	$nom_initiale = "i-j";
			      						  }
			      						  
			      						  if (in_array($nom_initiale, array("k","l"))) {
			      						  	$nom_initiale = "k-l";
			      						  }
			      						  
			      						  if (in_array($nom_initiale, array("o","p"))) {
			      						  	$nom_initiale = "o-p";
			      						  }
			      						  
			      						  if (in_array($nom_initiale, array("v","w","x","y","z"))) {
			      						  	$nom_initiale = "v-w-y-z";
			      						  }
			      						  		
			      						  		if ($nom_initiale != $prev_initiale) {
			      						  					      						  		
			      						  			if ($initialcounter != 1) {
			      						  					echo '</ul></div>'; // close previous initiale
			      						  			}
			      						  			
			      						  			?><div class="bloc-initiale">
			      						  			
			      						  				<h2 class="h2 initiale" id="noms-<?php echo $nom_initiale; ?>"><span class="initiale-text"><?php echo $nom_initiale; ?></span></h2>
			      						  				
			      						  				 <ul class="ul-initiale clean unstyled rel">
			      						  				 <?php
			      						  		} 
			      						  
			      						   ?>
			      						   <li class="li">
			      						   <a href="<?php 
			      						   
			      						   echo $array_membres[$key]["permalink"];
			      						   
			      						    ?>"><?php 
			      						   
			      						   // $nom_prenom = get_the_title();
			      						   $nom_prenom = vlrd_nom_prenom($nom_entier);
			      						   echo $nom_prenom;
			      						   
			      						   $prev_initiale = $nom_initiale;
			      						   $initialcounter++;
			      						   
			      						   ?></a>
			      						  </li>
	      						  
	      				  <?php
	      				  		} 
	      				  		
	      				  	// close the final "bloc-initiale"
	      				  	echo '</ul></div>';
	      				  ?>
	      		</ul>
	      		<?php
	      		endif;
      		
      		?>
      		</div><!-- .li-lo-item -->
    	</div><!-- .li-lo list -->
    	
  </div>
  

</div>

<?php get_footer(); ?>
