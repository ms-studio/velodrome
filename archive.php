<?php
/**
 */

get_header(); 

$vldr_section_class = "main-section";

// echo $query_string; 

?>

<div id="main" class="main" role="main">

  <?php if (have_posts()) : ?>
  
  <?php 
  	  
  	if (is_tax()) {
  			
  			/** Background Images
  			    ***************************
  			     */
  			     
  			    // test for attached images
  			    
  			    $queried_object = get_queried_object();
  			
  			    $acf_term_img = get_field('acf_lieux_img', $queried_object);
  			    
  			    // test for JPEG and generate background.
  			    vldr_generate_background($acf_term_img,$queried_object);
  			    
  			    
  			    /** Background Map
  			    ***************************
  			     */

  			    // test for SVG and generate background.
  			    
  			    if ( isset ( $acf_term_img ) ) {
  			    
  			    		$vldr_svg = vldr_generate_svg( $acf_term_img, $queried_object );
  			    		
  			    		if ( !empty ( $vldr_svg ) ) {
  			    				
  			    				// vldr_svg_display($vldr_svg);
  			    				
  			    				if ( isset( $vldr_svg[0]["url"] ) ) {
  			    					
  			    						?>
  			    						<style>
  			    						.main-section {
  			    							background-image: url('<?php echo $vldr_svg[0]["url"]; ?>');
  			    							background-position: 0% 0%;
  			    							background-repeat: no-repeat;
  			    							background-size: 100% auto;
  			    							background-origin: padding-box;
  			    						}
  			    						</style>
  			    						<?php
  			    						
  			    						$vldr_section_class .= " has-svg";
  			    					
  			    					} // isset
  			    		
  			    		} // !empty
  			    		
  			    } //isset
  			    
  			    /** end Background Map
  			     */
  			    
  	
  	} // if is_tax()
  
   ?>
  <section class="<?php echo $vldr_section_class; ?>">
    <?php 
    
    $headr_open = '<h1 class="h1 article-title"><span class="title-style">';
    $headr_close = '</span></h1>';
    $current_term = single_term_title("", false);
    
    /* If this is a category archive */ if ( is_category() ) { 
     
		     echo $headr_open.'Category: ';
		     echo $current_term;
		     echo $headr_close; 
     
      /* If this is a tag archive */ } elseif( is_tag() ) { 
      
		    echo $headr_open;
		    echo $current_term;
		    echo $headr_close; 
      
      /* If this is a Taxonomy */ } elseif ( is_tax('locaux') ) { 
          
		    echo $headr_open;
	    			echo '<a href="'.get_option('home').'" class="unstyled">';
	    			$current_term = vlrd_nom_local($current_term);
	    			echo $current_term;
	    			echo '</a>';
		    echo $headr_close; 
    
    /* If this is a Taxonomy */ } elseif ( is_tax() ) { 
    
    		echo $headr_open;
    		echo $current_term;
    		echo $headr_close;
    
    /* If this is a daily archive */ } elseif ( is_day() ) { ?>
    <h2 class="pagetitle">archive pour <?php the_time('F jS, Y'); ?></h2>
    <?php /* If this is a monthly archive */ } elseif ( is_month() ) { ?>
    <h2 class="pagetitle">archive pour <?php the_time('F, Y'); ?></h2>
    <?php 
    
    /* If this is a yearly archive */ } elseif ( is_year() ) { 
    	
    		$archive_year = $query_string;
    		$archive_year = substr($archive_year, 5);
    	
    		echo $headr_open;
    		echo $archive_year;
    		echo $headr_close; 
    
    /* If this is an author archive */ } elseif ( is_author() ) { ?>
    <h2 class="pagetitle">archive par auteur</h2>
    <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
    <h2 class="pagetitle">archives</h2>
    <?php } ?>

		<?php 
		
		// show tag description
		
		/*
		 * Show taxonomy description
		 * and custom meta
		*/
		
		//  show an optional tag description
		$queried_object = get_queried_object();
		
		// $term_id = $queried_object->term_id;
		
		$tag_description = $queried_object->description;
		
		if ( ! empty( $tag_description ) ) {
			echo '<div class="taxonomy-description fullwidth-content single-content">';
			echo apply_filters( 'the_content', $tag_description );
			echo '</div>';
		}
		
		
		if ( is_year() ) {
					
					while (have_posts()) : the_post();
						include( get_template_directory() . '/inc/news-item-create.php' );
					endwhile;
					
					if (!empty($archive_array)) {
							?>
								<section class="arch-sect">
								<?php
						
								global $mem_today_short;
								
								$archive_type = 'default';		
										
								foreach ($archive_array as $key => $item) {
										
										include( get_template_directory() . '/inc/news-item.php' );
										
								} // foreach
																	
								echo '</section>';
								
								/*************** 
									 *** ARCHIVES ***
									*************** */
									
								include( get_template_directory() . '/inc/yearly-archives.php' );
							
					} // !empty($archive_array)
		
		} else if ( is_tax('locaux') ) {
		
					$collectifs_ids = array();
		
				?>
				<ul class="ul-membres ul-clean">
				
		    <?php while (have_posts()) : the_post(); 
		    
		    		// test for Collectif taxonomy.
		    		
		    		$liste_collectifs = get_the_terms( $post->ID, 'collectifs' );
		    		
		    		if ($liste_collectifs) {
		    		
		    				
		    					    			
		    		   	foreach ( $liste_collectifs as $collectif ) {
		    		   		
		    		   		if (in_array( $collectif->term_id, $collectifs_ids ) ) {
		    		   			// a duplicate
		    		   		} else {
		    		   		
			    		   		$nom_collectif = $collectif->name;
			    		   		$collectifs_ids[] = $collectif->term_id;
			    		   		
			    		   		echo '<li class="collectifs li">';
			    		   		echo '<a href="' . get_term_link( $collectif->term_id, 'collectifs' ) . '" class="unstyled">' . $nom_collectif . '</a>';
		    		   			echo '</li>';
		    		   		}
		    		   		
		    		   	} // foreach
		    		 	
		    		} // end if liste
		    
		    ?>
		      <li <?php post_class('li') ?>>
			        <a href="<?php the_permalink(); ?>"><?php 
			        
			        // the_title(); 
			        $nom_entier = get_the_title();
			        $nom_prenom = vlrd_nom_prenom($nom_entier);
			        echo $nom_prenom;
			        
			        ?></a>
		    </li>
		    <?php endwhile; ?>
		    
		    
		    </ul>
		    
		    <?php
		
		} else {
		
				?>
				<ul class="ul-membres ul-clean">
				
		    <?php while (have_posts()) : the_post(); ?>
		      <li <?php post_class('li') ?>>
			        <a href="<?php the_permalink(); ?>"><?php 
			        
			        // the_title(); 
			        $nom_entier = get_the_title();
			        $nom_prenom = vlrd_nom_prenom($nom_entier);
			        echo $nom_prenom;
			        
			        ?></a>
		    </li>
		    <?php endwhile; ?>
		    
		    </ul>
		    
		    <?php
    
    } //
    
     ?>

  </section>

  <?php else :

  if ( is_category() ) { // If this is a category archive
    printf("<h2 class=\"h2\">Sorry, but there aren't any posts in the %s category yet.</h2>", single_cat_title('',false));
  } else if ( is_date() ) { // If this is a date archive
    echo("<h2 class=\"h2\">Sorry, but there aren't any posts with this date.</h2>");
  } else if ( is_author() ) { // If this is a category archive
    $userdata = get_userdatabylogin(get_query_var('author_name'));
    printf("<h2 class=\"h2\">Sorry, but there aren't any posts by %s yet.</h2>", $userdata->display_name);
  } else {
    echo("<h2 class=\"h2\">Ooops, il n’y a rien sur cette page.</h2>");
  }
  // get_search_form();

  endif;
  ?>

</div>


<?php get_footer(); ?>
