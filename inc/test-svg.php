<?php

// test for SVG and generate background.
// note: not used anymore.
		
		if (isset($acf_term_img) && isset($local_array) ) {
		
				$vldr_svg = vldr_generate_svg($acf_term_img,$local_array);
				
				if (!empty($vldr_svg)) {
												
						if ( isset( $vldr_svg[0]["url"] ) ) {
							
								?>
								<style>
								.single-membre {
									background-image: url('<?php echo $vldr_svg[0]["url"]; ?>');
									background-position: left;
									background-repeat: no-repeat;
									background-size: 101% auto;
									background-origin: padding-box;
								}
								</style>
								<?php
								
								$vldr_post_class .= " has-svg";
							
							} else { 
								$vldr_post_class .= " no-svg";
							}
				
				} else { 
					$vldr_post_class .= " no-svg";
				}
				
		} else { 
			$vldr_post_class .= " no-svg";
		}