<?php 


$format = 'json';

// (optional) Either xml, json or js (for jsonp).
 

foreach($soundcloud as $sound_url) {
	
	trim($sound_url);
	
	$api_endpoint = 'http://soundcloud.com/oembed?format='.$format.'&url='.urlencode($sound_url).'&iframe=true&color=000000&auto_play=false&show_artwork=true';
	
			$oembed = curl_get($api_endpoint);
			
			$decode = json_decode($oembed);
			
			if ($decode == null || !$decode) {
				$error = true;
				echo '<p class="error-message">SoundCloud: some error occurred.</p>';
				//error control here
			} else {
				echo '<div class="media-container soundcloud-container">';
				print_r($decode->html);
				echo '</div>';
				
			}
	
} // end foreach


