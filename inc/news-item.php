<?php 
		
//		if ($archive_type == "past") {
//		
//				// check YEAR against previous
//				
//				if ( $previous_year == $archive_array[$key]["date-year"] ) {
//						// do nothing
//				} else {
//						
//						// redefine
//						$previous_year = $archive_array[$key]["date-year"];
//						echo '<h3>'.$previous_year.'</h3>';
//				}
//		
//		}



	// START MARKUP 
	
	echo '<div class="news-item clear';

				/** 
				 * show the image.
				 */
				
				if (empty($archive_array[$key]["url-custom"])) {
						
						// do nothing
						echo ' no-img">';
				
				} else {		
						
						echo ' has-img">';
						
						echo '<div class="news-item-img"><img src="'.$archive_array[$key]["url-custom"].'"></div>';
						
						// avec lien swipebox:
//						echo '<div class="news-item-img"><a href="'.$archive_array[$key]["url-large"].'" class="colorbox"><img src="'.$archive_array[$key]["url-custom"].'"></a></div>';
						
//						echo '<a href="'.$item["url-large"].'" class="colorbox img-gl-itm" data-caption="'.esc_attr($item["caption"]).'"><img width="'.$item["width-custom"].'" height="'.$item["height-custom"].'" src="'.$item["url-custom"].'" class="attachment-thumbnail" alt="'.esc_attr($item["alt"]).'" /></a>';

				}
				
				 ?>
					
				<div class="news-item-title">
				<?php 
				
				// Display connected people
				
				echo $archive_array[$key]["person-link"];
				
				 ?>	
				<h4 class="h4<?php 
				
				if (!empty($archive_array[$key]["person-link"])) {
				
					echo ' norm';
				}
				
				 ?>"><?php 
				 
				 // test if connected
				 
				 if ( is_user_logged_in() ) {
				   
				   ?><a href="<?php bloginfo('wpurl');?>/wp-admin/post.php?post=<?php echo $archive_array[$key]["id"]; ?>&action=edit"><?php echo $archive_array[$key]["title"]; ?></a><?php
				   
				   } else {
				   
				   		echo $archive_array[$key]["title"];
				   		
				   }

				 ?></h4>

		<div class="event-date-box strong">
		<?php 
					
					// Test if a publication date exists.
					
					if ($archive_array[$key]["date-pub"] == '') {
							
							// No = This means that an Event Date is defined.
		 ?>	
		 	<time class="event-date-box-inside smaller black" itemprop="startDate" datetime="<?php echo $archive_array[$key]["start-date-iso"] ?>">
		 	<?php 
		 	
		 	$date_string = $archive_array[$key]["start-date-iso"];
		 	
		 	// compare against TODAY... 
		 	global $mem_unix_now;
		 	          	
		 	if ( $date_string >= $mem_today_short ) { // Future Event
		 	
		 		echo $archive_array[$key]["date-string"]; 
		 	
		 		$unix_date = strtotime($date_string) ;
		 		$time_diff = ($unix_date - $mem_unix_now) ;
		 		
		 		// seconds to days : 60 * 60 * 24 = 86400
		 		$time_diff = floor($time_diff / 86400);
		 		
		 		if ( $time_diff <= 30) {
		 			echo ' (<span class="micron">&nbsp;</span>J<span class="micron">&nbsp;</span>−'.$time_diff.'<span class="micron">&nbsp;</span>)';
		 		}
		 		
		 	} else { // Not Future
		 	
		 		echo $archive_array[$key]["date-short"]; 
		 	}
		 	
		 	 ?>
			</time>
			<?php 
			
			} else { // No event date - normal publication date
				?>
				<time itemprop="datePublished" datetime="<?php echo $archive_array[$key]["start-date-iso"] ?>">
				<?php 
				
				echo $archive_array[$key]["date-short"]; 
				
				 ?>
				</time>
				<?php
			}// end testing for Pub-Date
			
			 ?>
		</div><!-- event-date-box -->
		
		<?php 
		
		
		// create edit post link
		if ( is_user_logged_in() ) {
		  
		  ?><span class="edit-link hidden"> [<a href="<?php bloginfo('wpurl');?>/wp-admin/post.php?post=<?php echo $archive_array[$key]["id"]; ?>&action=edit">modifier</a>]</span><?php
		  
		  }
		
		?>
		</div><!-- news-item-title -->
		
		<div class="news-item-content">
		<?php 
		
		// echo $archive_array[$key]["content"];
		
		$news_item_content = apply_filters('the_content',$archive_array[$key]["content"]);
		
//		$tiss_content = tiss_process_hyperlinks($tiss_content);
		
		echo $news_item_content;
		
		 ?>
		</div><!-- news-item-content -->
		
</div><!-- news-item -->

