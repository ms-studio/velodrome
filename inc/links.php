<?php 

/*

// may create $vimeo or $soundcloud arrays.

*/

// init variables
$soundcloud = array();
$vimeo = array();
$youtube = array();
$otherlinks = array();

$external_links =  get_post_meta($post->ID, 'acf_lien_externe_bloc', false);

// processing all types of external links:

if ($external_links) {
	 
	 		while(has_sub_field('acf_lien_externe_bloc')): 
	 		
	 				$testlink = get_sub_field('acf_lien_externe');
	 				
	 				// we remove HTTP for better analysis.
	 				
	 				$testlink = str_replace("http://", "", $testlink);
	 				$testlink = str_replace("https://", "", $testlink);
	 				$testlink = str_replace("www.", "", $testlink);
	 				
	 				/* 
	 				 * analyse each get_sub_field('acf_lien_externe')
	 				**/
	 						
	 				if (substr($testlink, 0, 9) == "vimeo.com")  { 
	 							$testlink = 'https://'.$testlink;
	 							// add to $vimeo array
	 							$vimeo[] = $testlink;
	 							
	 				} else if (substr($testlink, 0, 11) == "youtube.com")  { 
	 								$testlink = 'http://www.'.$testlink;
	 								// add to $soundcloud array
	 								$youtube[] = $testlink;
	 					 				
	 				} else if (substr($testlink, 0, 14) == "soundcloud.com")  { 
									$testlink = 'https://'.$testlink;
									// add to $soundcloud array
									$soundcloud[] = $testlink;
	 				
	 				} else { 
	 							// add to $other_links array
	 							$otherlinks[] = $testlink;
	 							// echo '<p>'.$testlink.'</p>';
	 				}
	 		 							
	 		endwhile; 
	 
}    




	

		 
