<?php 


// (optional) Either xml, json or js (for jsonp).
 
//$decode = json_decode($json, true);


foreach($vimeo as $vimeo_url) {
		
		trim($vimeo_url);
		
		$vimeo_test = discoverVimeo($vimeo_url);
		
//		echo '<pre>';
//		var_dump($vimeo_test);
//		echo '</pre>'; 	
		
		$vimeo_type = $vimeo_test["type"];
		$vimeo_id = $vimeo_test["id"];
		
		// TEST FOR TRANSIENT
		// if we have it, generate output.
		
		// delete_transient( 'vimeo_'.$vimeo_type.'_'.$vimeo_id );
	
	if ( false === ( $videos = get_transient('vimeo_'.$vimeo_type.'_'.$vimeo_id) ) ) {	

//		  	echo '<p> transient vimeo_'.$vimeo_type.'_'.$vimeo_id.' is redefined </p>';
		
		// if transient not defined, query VIMEO.COM
				
		if ($vimeo_type == 'channel') {
		
			$api_endpoint = 'http://vimeo.com/api/v2/channel/'.$vimeo_id.'/videos.xml';
			
		} else if ($vimeo_type == 'album') {
		
			$api_endpoint = 'http://vimeo.com/api/v2/album/'.$vimeo_id.'/videos.xml';
			
		} else if ($vimeo_type == 'item') { // single video item
		
			$api_endpoint = 'http://vimeo.com/api/v2/video/'.$vimeo_id.'.xml';
		
		} else if ($vimeo_type == 'user') { // user pge
		
			// $api_endpoint = 'http://vimeo.com/api/v2/video/'.$vimeo_id.'.xml';
			$api_endpoint = 'http://vimeo.com/api/v2/'.$vimeo_id.'/videos.xml';
		
		}
		
				$video_xml_data = simplexml_load_string(curl_get($api_endpoint));
				
				if (!$video_xml_data) {
				  echo "Erreur de chargement";
				}
				
				if (!empty($video_xml_data)) {
						
						// transform into array, so we can store it as transient.
						$videos = json_decode( json_encode($video_xml_data) , 1);
						
//						echo '<pre class="hidden"> $videos: ';
//						var_dump($videos);
//						echo '</pre>'; 
						
						if ($vimeo_type == 'user') {
							
							// are there one or several videos?
							
							if ( isset( $videos["video"][1]) ) {
								// several videos
								$videos = $videos["video"];
							} else {
								// only one video
								$videos[0] = $videos["video"];
							}
							
						}
						
//						echo '<pre class="hidden"> $videos: ';
//						var_dump($videos);
//						echo '</pre>'; 
						
						set_transient( 'vimeo_'.$vimeo_type.'_'.$vimeo_id, $videos , 12 * HOUR_IN_SECONDS ); 
						
						// 12 * HOUR_IN_SECONDS 
						// MINUTE_IN_SECONDS
						
				} // end testing if !empty 
				
		} // end testing for the Transient
		
		// NOW : GENERATE OUTPUT.
		
		   $vimcounter = 0;
		   
		   if ( empty($vimeo_howmany) ) {
		   			$vimeo_howmany = 1;
		   		}
		   
		   // test if the array exists 
		   if (!empty($videos)) {
		   
//		   		echo '<pre class="hidden"> $videos: ';
//		   		var_dump($videos);
//		   		echo '</pre>'; 	
//		   
		   // NOW RUN THE LOOP
		   
		   			foreach ($videos as $row => $item) { 
		   					   			
		   			if(++$vimcounter > $vimeo_howmany) break; // maximum of 5 results
		   			
		   					$vid_height = $item["height"];
		   					$vid_width = $item["width"];
		   					
		   					if (!empty($vid_width)) {
		   						$vid_ratio = ($vid_height / $vid_width)*100;
		   					}
	
		   					$player_url = 'http://player.vimeo.com/video/' . $item["id"] . '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1';	
		   					// $player_url = $vimeo_url . '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1';
		   				
		   				?>
		   				<div class="media-container vimeo-container">
		   				<div class="vimeo-inside" style="background-image: url(<?php 
		   							echo $item["thumbnail_large"] ;
		   				?>);" data-ratio="<?php echo $vid_ratio; ?>">
		   				
		   				<a href="<?php echo $player_url; ?>" data-vimeo="<?php echo $item["id"]; ?>" target="_blank" title="<?php echo $item["title"]; ?>" data-caption="<?php echo $item["title"]; ?>" class="vimeo-img-link vimeoframe unstyled" style="padding-bottom: <?php 
		   						echo $vid_ratio; ?>%">
		   					
		   						<div class="vimeo-play-icon">Play!</div>
		   					
		   						<img class="vimeo-still" src="<?php 
		   						
		   						echo $item["thumbnail_large"] ;
		   						
		   						?>" alt="" />
		   					
		   					<div class="vid-legende">
		   					  <p class="img-title vid-title"><?php 
		   						
		   							echo $item["title"] ;
		   											
		   					?></p> 
		   					</div><!-- div.img-legende -->
		   				</a><!-- .vimeoframe -->
		   				
		   				  </div></div><!-- .vimeo-container -->
		   				
		   				<?php
		   						   				
		   			} // end Vimeo Foreach 
		   
		} // end test !empty
		
} // end foreach

?>