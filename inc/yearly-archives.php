<?php

/*************** 
	 *** ARCHIVES ***
	*************** */

echo '<section class="arch-sect">';

echo '<h2 class="h2">archives</h2>';
echo '<ul class="year-archive clean">';

// show list of years.

$args = array(
	'type'            => 'yearly',
	'limit'           => '',
	'format'          => 'html', 
	'before'          => '',
	'after'           => '',
	'show_post_count' => false,
	'echo'            => 1,
	'order'           => 'DESC'
);

wp_get_archives( $args );

echo '</ul></section>';