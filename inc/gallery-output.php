<?php

// ********* Produce Image Gallery **********  
    
    global $has_gallery;
    global $img_info;
    
    if ($has_gallery == true) { 
    	
    			// echo '<section class="sub-content img-gallery">';
    					
    			// echo '<div class="clear img-gallery-content">';
    				
    			foreach ($img_info as $key => $item){
		    			
	    				echo '<a href="'.$item["url-large"].'" class="colorbox img-gl-itm" data-caption="'.esc_attr($item["caption"]).'" style="background-image:url('.$item["url-medium"].')" title="'.esc_attr($item["title"]).'">';	
	    				echo '<img width="'.$item["width-medium"].'" height="'.$item["height-medium"].'" src="'.$item["url-medium"].'" class="attachment-thumbnail" alt="'.esc_attr($item["alt"]).'" />';
	    				echo '</a>';
				    							    			
		    		} // end foreach
    			
    			
    			
    			if ($galerie_credits) {
    			
    				echo '<footer class="credits-photo"><p class="small-font">';
    				
    				echo 'Photographie: '.$galerie_credits;
    					
    				echo '<p></footer>';
    			}
    			
    			
    }
    	
// end Images.
        
