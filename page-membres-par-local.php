<?php
/**
 * Template Name: Liste des membres (par local)
 */

get_header(); ?>

<div id="main" role="main" class="main page-membres">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div class="post" id="post-<?php the_ID(); ?>">
    <header>
      <h1 class="h1"><?php the_title(); ?></h1>
    </header>
  
    <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); 
    
    // listing of Tags of taxonomy "locaux".
    
    ?>
    
    <div class="li-lo">
      	<?php 
      	
      	// li-lo = liste locaux
      	      		
      	   $args = array(
      	   	'type'                     => 'post',
      	   	'child_of'                 => 0,
      	   	'parent'                   => 0,
      	   	'orderby'                  => 'name',
      	   	'order'                    => 'ASC',
      	   	'hide_empty'               => 0,
      	   	'hierarchical'             => 1,
      	   	'exclude'                  => '',
      	   	'include'                  => '',
      	   	'number'                   => '',
      	   	'taxonomy'                 => 'locaux',
      	   	'pad_counts'               => false 
      	   
      	   );
      	   
      	   // see http://codex.wordpress.org/Function_Reference/get_categories
      	   
      	   // hierarchical = When true, the results will include sub-categories that are empty, as long as those sub-categories have sub-categories that are not empty.
      	   
      	   // parent = when the value is set to zero, we get the top level categories only.
      	   
      	$liste_des_locaux = get_categories( $args );
      	
      	
      	
      	foreach ( $liste_des_locaux as $local ) {
      		
      		$slug_local = $local->slug;
      		$nom_local = $local->name;
      		
      		$nom_local = vlrd_nom_local($nom_local);
      		      		
      		echo '<div class="li-lo-item">';
      		echo '<h2 class="h2 li-lo-h"><a href="' . get_term_link( $slug_local, 'locaux' ) . '">' . $nom_local . '</a></h2>';
      			
      		// query for linked articles of post type = "membre".
      				
      				$custom_query = new WP_Query( array(
      					      		 		'post_type' => array( 'membres' ),
      					      		 		'tax_query' => array(
      					      		 				array(
      					      		 					'taxonomy' => 'locaux',
      					      		 					'field' => 'slug',
      					      		 					'terms' => $slug_local
      					      		 				)
      					      		 		),
      					      		 		'posts_per_page' => -1,
      					      		 		'orderby'  => 'title',
      					      		 		'order'  => 'ASC', // DESC means: newest first
      					      		 		));
      					      		
      					      		if ( $custom_query->have_posts() ) :
      					      				  	// YES, we have something ...
      					      				?>
      					      				<ul class="ul-membres ul-clean">
      					      				<?php 
      					      				  	
      					      						  while( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
      					      						  <li class="li">
      					      						   <a href="<?php the_permalink(); ?>"><?php 
      					      						   
      					      						   $nom_prenom = get_the_title();
      					      						   $nom_prenom = vlrd_nom_prenom($nom_prenom);
      					      						   echo $nom_prenom;
      					      						   
      					      						   ?></a>
      					      						  </li>
      					      						  
      					      				  <?php
      					      				  		  endwhile; 
      					      				  ?>
      					      		</ul>
      					      		<?php
      					      		endif;
      					      		wp_reset_postdata();
      		
      		echo '</div>'; // end .li-lo-item
      		
      	} // end foreach
      				 
      	?>
    	</div>
    	
  </div>
  <?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
