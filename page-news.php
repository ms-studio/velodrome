<?php
/**
 * Template Name: News
 */

get_header(); ?>

<div id="main" role="main" class="main">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <article class="post" id="post-<?php the_ID(); ?>">
    <header class="main-header">
      <h1 class="h1"><?php the_title(); ?></h1>
    </header>
  
    <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
  
 
  <?php endwhile; endif; 
  
  		
  		// delete_transient( 'page_news' );
  		
  		  if ( false === ( $archive_array = get_transient('page_news') ) ) {
  		  	
  		
  				$exclude_id = array();
  		  	
  		  	// Loop 1: items with EVENT DATE
  		  	
  		  	$current_year = date("Y");
  		  	
  		  	$has_date_query = new WP_Query( array( 
  		  				'posts_per_page' => -1,
  		  				'category_name' => 	'news',
  		  				'meta_query' => array(
  		  				    'relation' => 'AND',
  		  				    array(
  		  				        'key' => '_mem_start_date',
  		  				        'value' => $current_year,
  		  				        'compare' => '>=',
  		  				    ),
  		  				    array(
  		  				        'key' => '_mem_start_date',
  		  				        'value' => $current_year.'-12-32',
  		  				        'compare' => '<=',
  		  				    ),
  		  					)
  		  				));
  		  	
  		  	if ( $has_date_query->have_posts() ) :
  		  			  while( $has_date_query->have_posts() ) : $has_date_query->the_post();
  		  					  		  					 
  		  					 include( get_template_directory() . '/inc/news-item-create.php' );
  		  		 				    			 				
  		  		 endwhile; 
  		  	endif; 
  		  		     	     
  		  	// END of first loop	
  		  	
  		  	// 2: Query posts with NO event date
  		  	// TODO: restrict by publication year = current year
  		  		
  		  		$no_date_query = new WP_Query( array(
  		  			 	'posts_per_page' => -1,
  		  			 	'post__not_in' => $exclude_id,
  		  			 	// 'category_name' => 'news',
  		  			 	'category_name' => 'something',
  		  			 	'orderby' => 'date',
  		  			 	'order' => 'DESC', 
  		  			 	'year' => $current_year,
  		  			 	) ); 
  		  		    			 
  		  	if ( $no_date_query->have_posts() ) :
  		  			  while( $no_date_query->have_posts() ) : $no_date_query->the_post();
  		  			 
  		  			 			include( get_template_directory() . '/inc/news-item-create.php' );
  		  			 			    			 			
  		  				endwhile; 
  		  	endif; 
  		  	
  		  	// SORT everything by "start-date-iso"
  		  	if (!empty($archive_array)) {
  		  			
  		  			function multi_array_sort($a,$b) {
  		  			     return $a['end-date-iso']<$b['end-date-iso'];
  		  			}
  		  			usort($archive_array, "multi_array_sort");
  		  	}		
  		  	set_transient( 'page_news', $archive_array, 12 * HOUR_IN_SECONDS ); 
  		  	
  		  	// use MINUTE_IN_SECONDS, HOUR_IN_SECONDS
  		  	
  		  	} // end of get_transient test
  		  	
  		  	
  		  	/*************** 
  		  	 *** OUTPUT ***
  		  	*************** */
  		  	
  		  	if (!empty($archive_array)) {
  		  			?>
  		  				<section class="arch-sect">
  		  				<?php
  		  		
  		  		global $mem_today_short;
  		  		
  		  		$archive_type = 'default';		
  		  				
  		  		foreach ($archive_array as $key => $item) {
  		  				
  		  				include( get_template_directory() . '/inc/news-item.php' );
  		  				
  		  		}
  		  			echo '</section>';
  		  	}
  		
  				/*************** 
  				 *** ARCHIVES ***
  				*************** */
  			
  				include( get_template_directory() . '/inc/yearly-archives.php' );  		
  					
  ?>
   </article>

</div>

<?php get_footer(); ?>
