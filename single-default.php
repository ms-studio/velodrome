<?php 

		/*******************
			* Test Metadata *
		********************/
	
		// Tester la date-événement
		
		 $mem_date = mem_date_processing( 
		 	get_post_meta($post->ID, '_mem_start_date', true) , 
		 	get_post_meta($post->ID, '_mem_end_date', true)
		 );
		
		$current_post_id = get_the_ID();
		 $exclude_id[] = $current_post_id;
		 
		 // IMAGES
		 
		 $img_info = gallery_init('medium');
		   		  					 
		 if ( empty( $img_info ) ) { 
		 	$img_url_custom = '';
		 	$img_url_large = '';
		 } else { // not empty!
		 	$img_url_custom = $img_info[0]["url-custom"];
		 	$img_url_large = $img_info[0]["url-large"];
		 }
		 	
		 // DATE
		
		 $mem_date = mem_date_processing( 
		 	get_post_meta($current_post_id, '_mem_start_date', true) , 
		 	get_post_meta($current_post_id, '_mem_end_date', true)
		 );
		 
		 /*
		  * Prepare date values.
		  * Event date or publication date?
		 */
		 
		 if ($mem_date["start-iso"] !=="") { 
		 
		 			// case 1: use MEM date
		 					$date_string = $mem_date["date"];
		 					$date_short = $mem_date["date-short"];
		 					$date_year = $mem_date["start-year"];
		 					
		 					$start_date_iso = $mem_date["start-iso"];  // 2013-03-11T06:35
		 					$end_date_iso = $mem_date["end-iso"];
		 					$unix_start = $mem_date["start-unix"];
		 					$unix_end = $mem_date["end-unix"];
		 					$has_event_date = true;
		 			 
		   } else {
		   
		   	// case 2: no MEM date defined - use POST date
		   				
		   				$date_string = get_the_date( 'l j F Y' ); // Mercredi 5 juin 2013
		   				$date_short = get_the_date( 'F Y' );
		   				$date_year = get_the_date( 'Y' );
		   				
		   				$start_date_iso = get_the_date( 'Y-m-d\TH:i' );  // 2013-03-11T06:35
		   				$end_date_iso = $start_date_iso;
		   				$unix_start = strtotime( $start_date_iso );
		   				$unix_end = $unix_start;
		   				$has_event_date = false;
		   				
		   }
		   
		   /* PUT INTO ARRAY!
		    **********************
		   */
		 
		 $archive_array[] = array( 
		     	"id" => $current_post_id,
		     	"permalink" => get_permalink(),
		     	"title" => get_the_title(),
		     	"content" => get_the_content(),
		     	// IMAGES
		     	"url-custom" => $img_url_custom,
		     	"url-large" => $img_url_large,
		     	// DATES
		     	"date-string" => $date_string,
		     	"date-short" => $date_short,
		     	"date-year" =>  $date_year,
		     	"date-pub" => get_the_date( 'l j F Y' ),
		     	
		     	"start-date-iso" => $start_date_iso,
		     	"end-date-iso" => $end_date_iso,
		     	
		     	"start-date-unix" => $unix_start,
		     	"person-link" => vlrd_news_p2p(),
		    );
		
?>

  <article <?php post_class('single-article single-default') ?> id="post-<?php the_ID(); ?>">
  
  <header class="main-header">
        <h1 class="h1"><?php echo $archive_array[0]["title"]; ?></h1>
      </header>
  
  <?php 
  
  /*************** 
   *** OUTPUT ***
  *************** */
  
  if (!empty($archive_array)) {
  		?>
  			<section class="arch-sect">
  			<?php
  	
  	global $mem_today_short;
  	
  	$archive_type = 'default';		
  			
  	foreach ($archive_array as $key => $item) {
  			
  			include( get_template_directory() . '/inc/news-item.php' );
  			
  	}
  		echo '</section>';
  }
  
   ?>
    
  </article>

<?php

