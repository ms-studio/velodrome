<?php 


		$current_id = get_the_ID();
		
		$vldr_post_class = "single-article single-membre clear";
		
		/*******************
			* Test Metadata *
		********************/
		
		// Tester les locaux
		
		$liste_des_locaux = get_the_terms( $post->ID, 'locaux' );
		
		if ($liste_des_locaux) {
		
				$i = 0;
				
		  	foreach ( $liste_des_locaux as $local ) {
		  			
		  			if($i++ == 0) {
		  			
			  			// get attached background images
			  			
			  			$acf_term_img = get_field('acf_lieux_img', 'locaux_' . $local->term_id);
			  			
			  			$local_array = $local;
			  			
		  			}
		  	} // end foreach $local
		} // end if $liste_des_locaux
		
		
		// test for JPEG and generate background.
		// 
		
		if ( isset( $acf_term_img ) && isset( $local_array ) ) {
		
				vldr_generate_background( $acf_term_img, $local_array );
		
		}
		
		// test for SVG and generate background (removed)
		$vldr_post_class .= " no-svg";
		
		// Tester les images attachées
		
		include( get_template_directory() . '/modules/img-gallery/gallery-init.php' );
		
		// $img_info = gallery_init('medium');
		
		
		// test for custom fields 
		
		include( get_template_directory() . '/inc/links.php' );
		
		/* NOTE:
		*  this is testing for links.
		*  may create $headlinks / $soundcloud / $otherlinks arrays.
		 */
		
		$acf_courriel = get_field( "acf_courriel" );
    	 

?>

  <article <?php post_class( $vldr_post_class ) ?> id="post-<?php the_ID(); ?>">	
    <header class="main-header">
    
    	<?php 
    	
    	 if ($liste_des_locaux) {
    	 
    	 		echo '<div class="local"><h2 class="h2 li-lo-h">';
    				
    				$i = 0;
    				
			    	foreach ( $liste_des_locaux as $local ) {
			    		
			    		// may test first for Parent Element.
			    		
			    		$nom_local = $local->name;
			    		$nom_local = vlrd_nom_local($nom_local);
			    		
			    		if($i++ == 0) { 
			    				$loc_class = "loc-first"; 
			    		} else {
			    				$loc_class = "loc-more";
			    		}
			    		
			    		echo '<a href="' . get_term_link( $local->term_id, 'locaux' ) . '" class="'.$loc_class.'">' . $nom_local . '</a>';
			    		
			    	} // foreach $local
		    	
		    	echo '</h2></div>';
		    	
		   } // end if liste_des_locaux
    				 
    	 ?>
    
      <h1 class="h1 nom-prenom"><?php 
      
      echo vlrd_nom_prenom( get_the_title() );
      
      ?></h1>
    
    <?php 
    
    /* Display Collectifs
     ***********************/
     
    $liste_collectifs = get_the_terms( $post->ID, 'collectifs' );
    
    if ($liste_collectifs) {
    
    		echo '<div class="collectifs"><h3 class="h3">';
    			    			
       	foreach ( $liste_collectifs as $collectif ) {
       		
       		// may test first for Parent Element.
       		
       		$nom_collectif = $collectif->name;
       		
       		echo '<a href="' . get_term_link( $collectif->term_id, 'collectifs' ) . '" class="unstyled">' . $nom_collectif . '</a>';
       		
       	} //
     	
     	echo '</h3></div>';
     	
    } // end if liste
    
    
    
   	/* Display Activité
   	 ***********************/
    
    $acf_activite = get_field( "acf_activite" );
    
    if ($acf_activite) {
    	
//    		echo '<div class="acf-activite">';
      	echo '<p class="acf-activite bold">' . $acf_activite. '</p>';
//      	echo '</div>';
    }
    
    ?>
    </header>
    <?php
    
    $post_content = get_the_content();
    
    if ($post_content != '') {
    
    		echo '<div class="post-content">';
    		echo apply_filters('the_content', $post_content);
    		echo '</div>';
    
    }
    
    // the_content('Read the rest of this entry &raquo;'); 
    
    /* Display Gallery
     ***********************/
     
     echo '<section class="sub-content img-gallery">';
    
    include( get_template_directory() . '/inc/gallery-output.php' );
    
    /* Display Soundcloud
     ***********************/
    
    if ($soundcloud) {
    	  include( get_template_directory() . '/inc/links-soundcloud.php' );
    }
    
    /* Display Youtube
     ***********************/
    
    if ($youtube) {
    	  include( get_template_directory() . '/inc/links-youtube.php' );
    }
    
    /* Display Vimeo
     ***********************/
    
    if ($vimeo) {
    	  include( get_template_directory() . '/inc/links-vimeo.php' );
    } 
    
    echo '</section>';
    
    /* Links
     ***********************/
     
    if ( $otherlinks || $acf_courriel ) {
    
    	echo '<div class="meta-fields">';
    			
			    if ($otherlinks) {
			    	  include( get_template_directory() . '/inc/links-other.php' );
			    } 
			    	 
			    
			    
			    if ($acf_courriel) {
			    	
			    	$acf_courriel = vldr_process_hyperlinks($acf_courriel);
			    	
			    		echo '<div class="acf-courriel">';
			      	echo '<p>' . $acf_courriel. '</p>';
			      	echo '</div>';
			    }
			    
			  echo '</div>';  
			    
			}
    
   
    ?>
    
  </article>

<?php

