jQuery(document).ready(function($){

/*
 * CONTENTS
 *
 *   1: Email Spam Protection 
 *
 */
 
 
/* 
 * 0: js-hidden must be hidden
 ****************************************************
 */
// $(".js-hidden").hide(); // adds in-line CSS, hard to override.
$(".js-hidden").addClass( "hidden" );


/* 
 * Outgoing Links = new window
 ****************************************************
 */

$("a[href^=http]").each(
   function(){
      if(this.href.indexOf(location.hostname) === -1) {
      $(this).attr('target', '_blank');
			}
   }
 );

/* 
* Menu Toggle - 768 px is breakpoint
****************************************************
*/

if (document.documentElement.clientWidth < 768) {
//$(document).ready(function(){
    
    // open the menu
    $("h2#nav-menu-toggle").on("click", "span.menu-closed", function(){
    		 $(this).removeClass("menu-closed");
    		 $(this).addClass("menu-open");
    		 $('ul#menu-navigation-principale').removeClass("hidden-mob");
    		 return false;
     });
     
     // close the menu
     $("h2#nav-menu-toggle").on("click", "span.menu-open", function(){
     		 $(this).removeClass("menu-open");
     		 $(this).addClass("menu-closed");
     		 $('ul#menu-navigation-principale').addClass("hidden-mob");
     		 return false;
      });
     
//}); // function
} // 768px


 
/* 
* Search Form
****************************************************
*/

$( '.search-toggle' ).on( 'click', function( event ) {
			
			// alert('working');
			var that = $( this ),
			wrapper = $( '.main-search' );

			that.toggleClass( 'active' );
			wrapper.toggleClass( 'hidden' );

			if ( that.is( '.active' ) || $( '.search-toggle .screen-reader-text' )[0] === event.target ) {
				wrapper.find( '.searchfield' ).focus();
			}
			
			return false;
} );

 

/* 
 * 1.
 * EmailSpamProtection (jQuery Plugin)
 ****************************************************
 * Author: Mike Unckel
 * Description and Demo: http://unckel.de/labs/jquery-plugin-email-spam-protection
 * HTML: <span class="email">info [at] domain.com</span>
 */
$.fn.emailSpamProtection = function(className) {
	return $(this).find("." + className).each(function() {
		var $this = $(this);
		var s = $this.text().replace(" [at] ", "&#64;");
		$this.html("<a href=\"mailto:" + s + "\">" + s + "</a>");
	});
};
$("body").emailSpamProtection("email");


/* 
 * 
 * SwipeBox (jQuery Plugin)
 ****************************************************
 */
 
$("a>img.alignnone, a>img.aligncenter").parent().addClass("swipe");
 
$(".swipebox, .colorbox, .gallery-icon a, .wp-caption a, a.swipe").swipebox({
		useCSS : true, // false will force the use of jQuery for animations
		useSVG : true, // false to force the use of png for buttons
		hideBarsOnMobile : false, // false will show the caption and navbar on mobile devices
		hideBarsDelay : 10000, // delay before hiding bars
		videoMaxWidth : 1140, // videos max width
		beforeOpen: function() {}, // called before opening
		afterClose: function() {} // called after closing
	} );


/* 
 * 
 * Animate Velodrome Map
 ****************************************************
 */
 
 $( "#plan-velodrome" ).append( "<div id='plan-highlight' class='plan-highlight'></div>" );
 
// when hovering on .plan-lo-item
$("#plan-velodrome").on("mouseenter", "li.plan-lo-item", function(){
		 // alert( $( this ).text() );
// get top / left / height / widht of the a element
		 var styleProps = $( this ).css([
		     "width", "height", "top", "left"
		   ]);
// apply those values to the plan-highlight item.
		 $("#plan-highlight").css(styleProps);
//		 $(this).addClass("menu-open");
//		 $('ul#menu-navigation-principale').removeClass("hidden-mob");
//		 return false;
 });
 
 $("#plan-velodrome").on("mouseleave", "li.plan-lo-item", function(){
 // apply those values to the plan-highlight item.
 		 $("#plan-highlight").css({
 		       "width": "1%",
 		       "height": "%",
 		       "top": "2px",
 		       "left": "2px"
 		     });
  });


/* 
 * 
 * Play Vimeo
 ****************************************************
 */

$('a.vimeoframe').click(function(){ // the trigger action 
  
  var vimeokey = $(this).data('vimeo');
  
  // get the padding-bottom value
  var vimeoratio = $(this).css( "padding-bottom" );
  
  // apply it to the parent element
  $(this).parent("div.vimeo-inside").css( "padding-bottom", vimeoratio );
  
  $(this).replaceWith('<iframe src="http://player.vimeo.com/video/' + vimeokey + '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1" width="240" height="180" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen class="list-item-inside dblock"></iframe>');
  // $('.close-button').hide();
  return false;
});

/* 
 * that's it !
 ****************************************************
 */
		
}); // end document ready
		
		
		


// place any jQuery/helper plugins in here, instead of separate, slower script files.

